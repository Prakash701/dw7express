import express, { json } from "express";
import firstRouter from "./src/routes/firstRouter.js";
import schoolRouter from "./src/routes/schoolRouter.js";
import vehicleRouter from "./src/routes/vehicleRouter.js";
import connectToMongoDb from "./src/connectMongoDb.js";
import teacherRouter from "./src/routes/teacherRouter.js";
let expressApp = express();
expressApp.use(json()) //always make json after expressApp define
let port=8000





expressApp.use("/a",firstRouter) //localhost:8000/a
expressApp.use("/schools",schoolRouter) 
expressApp.use("/vehicles",vehicleRouter) 
expressApp.use("/teachers",teacherRouter) 
expressApp.listen(port, () => {
  console.log(`Application is connected to port ${port} sucessfully`);
});


connectToMongoDb()

/* */