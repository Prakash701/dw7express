import mongoose from "mongoose"

let dbUrl="mongodb://0.0.0.0:27017"
let connectToMongoDb=async()=>{
    try {
      await mongoose.connect(dbUrl)
      console.log(`Application is connected to mongodb at port ${dbUrl} successfully`)
    } catch (error) {
      console.log("Unable to connect mongose Db")
    }
    
  }
  export default connectToMongoDb