import { Router } from "express";

let schoolRouter = Router();

schoolRouter
.route("/")
.post((req,res,next)=>{
    console.log("1")
next()
},
(req,res,next)=>{
    console.log("2")
next()
},
(req,res,next)=>{  /* last middleware is called controller */
    console.log("3")}) 



.get((req,res)=>{
    res.json({
        success:true,
        message:"school read sucessfully"
    })
})

.patch((req,res)=>{
    res.json({
        success:true,
        message:"school update sucessfully"
    })
})
.delete((req,res)=>{
    res.json({
        success:true,
        message:"school delete sucessfully"
    })
})

schoolRouter
.route("/details")
.post((req,res)=>{
    res.json("hello") //always use one res
})


schoolRouter
.route("/:id1/name/:id2") //localhost:8000/schools/any -always use dynamic params at last
.post((req,res)=>{
    console.log(req.params)
    res.json({
        success:true,
        message:("this is params"),
        data:req.params
    })
})

export default schoolRouter;

/* middleware  is a function which has (req,res,next)
divided into 2 type based on error
1  normal middleware
define (req,res,next)=>{}
to trigger normal middleware next()

2  error middleware
define (err,req,res,next)=>{}
to trigger error middleware next("value")
 */

/* 
middleware  is a function which has (req,res,next)
divided into 2 type based on location
1 route level middleware

2 application middleware(used into index.js)

*/