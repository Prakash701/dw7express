import { Router } from "express";

let vehicleRouter = Router();

vehicleRouter
  .route("/")
  .post((req, res) => {
    res.json({
      success: true,
      message: "vehicle created sucessfully",
      data: req.body,
      query: req.query,
    });
  })

  .get((req, res) => {
    res.json({
      success: true,
      message: "vehicle read sucessfully",
    });
  })

  .patch((req, res) => {
    console.log(req.query);
    res.json({
      success: true,
      message: "vehicle update sucessfully",
      data: req.body,
    });
  })

  .delete((req, res) => {
    res.json({
      success: true,
      message: "vehicle delete sucessfully",
    });
  });

export default vehicleRouter;
