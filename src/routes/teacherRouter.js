import { Router } from "express";
import { Teacher } from "../schema/mode.js";

let teacherRouter = Router();

teacherRouter
  .route("/")

  .post(async (req, res) => {
    let data = req.body;

    try {
      let result = await Teacher.create(data);
      res.json({
        success: true,
        message: "teacher created sucessfully",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })

  .get(async (req, res) => {
    try {
      let result = await Teacher.find({});
      res.json({
        success: true,
        message: "teacher read sucessfully",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  });

teacherRouter
  .route("/:id")
  .get((req, res) => {
    let id = req.pqrqms.id;
    try {
      let result = Teacher.findById(id);
      res.json({
        success: true,
        message: "data read successfully",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })

  .patch(async (req, res) => {
    let id = req.params.id;
    let data = req.body;
    try {
      let result = await Teacher.findByIdAndUpdate(id, data, { new: true });
      res.json({
        success: true,
        message: "Teacher update sucessfully",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })

  .delete(async (req, res) => {
    let id = req.params.id;
    try {
      let result = await Teacher.findByIdAndDelete(id);
      res.json({
        success: true,
        message: "teacher delete sucessfully",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  });

export default teacherRouter;

/* 
Teacher.create(req.body)
Teacher.find({})
Teacher.findById(id)
Teacher.findByIdAndUpdate(id,req.body)
Teacher,findByIdAndDelete(id)
*/
