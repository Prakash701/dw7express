
import { model } from "mongoose";
import detailSchema from "./detailsSchema.js";
import teacherSchema from "./teacherSchema.js";
import bookSchema from "./bookSchema.js";
import studentSchema from "./studentSchema.js";
import classRoomSchema from "./classRoomSchema.js";
import departmentSchema from "./departmentSchema.js";
import collegeSchema from "./collegeSchema.js";

export let Detail=model("Detail",detailSchema)
export let Teacher=model("Teacher",teacherSchema)
export let Book=model("Book",bookSchema)
export let Student=model("Student",studentSchema)
export let ClassRoom=model("ClassRoom",classRoomSchema)
export let Department=model("Department",departmentSchema)
export let College=model("College",collegeSchema)