import { Schema } from "mongoose";

let departmentSchema = Schema({
  name: {
    type: String,
    required: true,
  },
  hod: {
    type: Number,
    required: true,
  },
  
  totalmember: {
    type: Boolean,
    required: true,
  },
});
export default departmentSchema