import { Schema } from "mongoose";

let classRoomSchema = Schema({
  name: {
    type: String,
    required: true,
  },
  numberofBench: {
    type: Number,
    required: true,
  },

  hasTv: {
    type: Boolean,
    required: true,
  },
});
export default classRoomSchema;
